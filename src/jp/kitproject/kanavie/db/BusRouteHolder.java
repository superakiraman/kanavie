package jp.kitproject.kanavie.db;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;


public class BusRouteHolder {
	private static final String TABLE_NAME = "BusRoute";

	private final DBAccessor dbConnection;

	public BusRouteHolder(Context context) {
		dbConnection = DBAccessor.getInstance(context);
		dbConnection.getWritableDatabase();
	}

	public boolean initialize(BusRoute[] data) {
		boolean result = true;

		SQLiteDatabase db = dbConnection.getWritableDatabase();

		try {
			db.beginTransaction();

			db.delete(TABLE_NAME, "", null);
			db.delete("RouteName", "", null);

			ContentValues values;
			for (BusRoute route : data) {
				values = new ContentValues();
				values.put("RouteID", route.getRouteId());
				values.put("Name", route.getRouteName());

				db.insert("RouteName", null, values);

				int[] stop = route.getBusStopList();
				for (int i = 0; i < stop.length; i++) {
					values = new ContentValues();
					values.put("RouteID", route.getRouteId());
					values.put("Number", i);
					values.put("BusID", stop[i]);

					db.insert(TABLE_NAME, null, values);
				}
			}

			db.setTransactionSuccessful();
		} catch (SQLException e) {
			result = false;
		} finally {
			db.endTransaction();
		}

		return result;
	}

	public BusRoute get(int routeId) {
		BusRoute result = null;

		SQLiteDatabase db = dbConnection.getReadableDatabase();

		Cursor datCursor = null;

		try {
			String routeName = "";
			ArrayList<Integer> tmp = new ArrayList<Integer>();
			datCursor = db.rawQuery("SELECT BusID, RouteName.Name FROM BusRoute " +
					"INNER JOIN RouteName ON (BusRoute.RouteID = RouteName.RouteID) " +
					"WHERE BusRoute.RouteID = ? ORDER BY Number;",
					new String[] { String.valueOf(routeId) });

			while (datCursor.moveToNext()) {
				tmp.add(datCursor.getInt(0));
				routeName = datCursor.getString(1);
			}

			int[] stop = new int[tmp.size()];
			for (int i = 0; i < tmp.size(); i++) {
				stop[i] = tmp.get(i);
			}

			if (stop.length > 0) {
				result = new BusRoute(routeId, stop, routeName);
			}
		} catch (SQLException e) {
		} finally {
			if (datCursor != null) { datCursor.close(); }
		}

		return result;
	}

	public BusRoute[] get(int start, int end) {
		ArrayList<BusRoute> result = new ArrayList<BusRouteHolder.BusRoute>();

		SQLiteDatabase db = dbConnection.getReadableDatabase();

		Cursor datCursor = null;

		try {
			ArrayList<Integer> tmp = new ArrayList<Integer>();

			datCursor = db.rawQuery("SELECT RouteID FROM BusRoute WHERE BusID = ?",
					new String[] { String.valueOf(start) });

			while (datCursor.moveToNext()) {
				tmp.add(datCursor.getInt(0));
			}

			for (int route : tmp) {
				datCursor = db.rawQuery("SELECT * FROM BusRoute WHERE RouteID = ? AND BusID = ?",
						new String[] { String.valueOf(route), String.valueOf(end) });

				if (datCursor.getCount() > 0) { result.add(get(route)); }
			}
		} catch (SQLException e) {
			Log.d("test", e.getMessage());
		} finally {
			if (datCursor != null) { datCursor.close(); }
		}

		return result.toArray(new BusRoute[result.size()]);
	}

	public class BusRoute {
		int routeId;
		int[] busStopList;
		String routeName;

		public BusRoute(int routeId, int[] busStopList, String routeName) {
			this.routeId = routeId;
			this.busStopList = busStopList;
			this.routeName = routeName;
		}

		public int getRouteId() {
			return routeId;
		}


		public int[] getBusStopList() {
			return busStopList;
		}


		public String getRouteName() {
			return routeName;
		}
	}
}
