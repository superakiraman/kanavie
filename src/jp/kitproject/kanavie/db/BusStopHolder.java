package jp.kitproject.kanavie.db;

import java.util.ArrayList;

import jp.kitproject.kanavie.BusStop;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class BusStopHolder {

	private static final String TABLE_NAME = "BusStop";

	private final DBAccessor dbConnection;

	public BusStopHolder(Context context) {
		dbConnection = DBAccessor.getInstance(context);
		dbConnection.getWritableDatabase();
	}

	public boolean initialize(BusStop[] busstop) {
		int complete = 0;
		boolean result = true;

		SQLiteDatabase db = dbConnection.getWritableDatabase();

		try {
			db.beginTransaction();

			db.delete(TABLE_NAME, "", null);

			ContentValues values;
			for (BusStop buss : busstop) {
				values = new ContentValues();
				values.put("ID", buss.getId());
				values.put("Name", buss.getName());
				values.put("Company", buss.getCompany());
				values.put("Latitude", buss.getLocation().getLatitude());
				values.put("Longtitude", buss.getLocation().getLongtitude());

				if (db.insert(TABLE_NAME, null, values) > 0) { complete++; }
			}

			db.setTransactionSuccessful();
		} catch (SQLException e) {
			result = false;
		} finally {
			db.endTransaction();
		}

		return result && (complete == busstop.length);
	}

	public BusStop get(int id) {
		BusStop result = null;

		SQLiteDatabase db = dbConnection.getReadableDatabase();

		Cursor datCursor = null;

		try {
			datCursor = db.query(TABLE_NAME,
					new String[] { "ID", "Name", "Company", "Latitude", "Longtitude" },
					"ID = ?", new String[] { String.valueOf(id) }, null, null, null);

			if (datCursor.moveToFirst()) {
				result = new BusStop(datCursor.getInt(0), datCursor.getString(1), datCursor.getString(2), datCursor.getDouble(3), datCursor.getDouble(4));
			}
		} catch (SQLException e) {
		} finally {
			if (datCursor != null) { datCursor.close(); }
		}

		return result;
	}

	public BusStop get(String name) {
		BusStop result = null;

		SQLiteDatabase db = dbConnection.getReadableDatabase();

		Cursor datCursor = null;

		try {
			datCursor = db.query(TABLE_NAME,
					new String[] { "ID", "Name", "Company", "Latitude", "Longtitude" },
					"Name = ?", new String[] { name }, null, null, null);

			if (datCursor.moveToFirst()) {
				result = new BusStop(datCursor.getInt(0), datCursor.getString(1), datCursor.getString(2), datCursor.getDouble(3), datCursor.getDouble(4));
			}
		} catch (SQLException e) {
		} finally {
			if (datCursor != null) { datCursor.close(); }
		}

		return result;
	}

	public BusStop[] getAll() {
		ArrayList<BusStop> result = new ArrayList<BusStop>();

		SQLiteDatabase db = dbConnection.getReadableDatabase();

		Cursor datCursor = null;

		try {
			datCursor = db.query(TABLE_NAME,
					new String[] { "ID", "Name", "Company", "Latitude", "Longtitude" },
					null, null, null, null,  "ID asc");

			while (datCursor.moveToNext()) {
				result.add(new BusStop(datCursor.getInt(0), datCursor.getString(1), datCursor.getString(2), datCursor.getDouble(3), datCursor.getDouble(4)));
			}
		} catch (SQLException e) {
		} finally {
			if (datCursor != null) { datCursor.close(); }
		}

		return result.toArray(new BusStop[result.size()]);
	}
}
