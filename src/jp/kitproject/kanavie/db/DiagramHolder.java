package jp.kitproject.kanavie.db;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;

import jp.kitproject.kanavie.BusDate;
import jp.kitproject.kanavie.db.BusRouteHolder.BusRoute;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;


public class DiagramHolder {
	private static final String TABLE_NAME = "Diagram";

	private final DBAccessor dbConnection;

	public DiagramHolder(Context context) {
		dbConnection = DBAccessor.getInstance(context);
		dbConnection.getWritableDatabase();
	}

	public boolean initialize(BusTimeData[] data) {
		boolean result = true;

		SQLiteDatabase db = dbConnection.getWritableDatabase();

		try {
			db.beginTransaction();

			db.delete(TABLE_NAME, "", null);

			ContentValues values;
			for (BusTimeData route : data) {
				BusDate[] time = route.getBusTimes();
				for (int i = 0; i < time.length; i++) {
					values = new ContentValues();
					values.put("RouteID", route.getRouteID());
					values.put("Number", route.getStopNumber());
					values.put("Week", route.getWeek());
					values.put("DiaNumber", i);
					values.put("Time", time[i].getDate());

					db.insert(TABLE_NAME, null, values);
				}
			}

			db.setTransactionSuccessful();
		} catch (SQLException e) {
			result = false;
		} finally {
			db.endTransaction();
		}

		return result;
	}

	public BusPlan[] get(int startId, int goalId, BusRoute route, BusDate nowTime) {
		ArrayList<BusPlan> result = new ArrayList<DiagramHolder.BusPlan>();;

		SQLiteDatabase db = dbConnection.getReadableDatabase();

		Cursor datCursor = null;

		try {
			ArrayList<Integer> tmp = new ArrayList<Integer>();
			datCursor = db.rawQuery("SELECT DiaNumber FROM Diagram " +
					"INNER JOIN BusRoute ON (Diagram.RouteID = BusRoute.RouteID AND Diagram.Number = BusRoute.Number) " +
					"WHERE Diagram.RouteID = ? AND BusRoute.BusID = ? AND Diagram.Time > ?",
					new String[] { String.valueOf(route.getRouteId()), String.valueOf(startId), String.valueOf(nowTime.getDate()) });

			while (datCursor.moveToNext()) {
				tmp.add(datCursor.getInt(0));
			}

			int[] diagramNumber = new int[tmp.size()];
			for (int i = 0; i < tmp.size(); i++) {
				diagramNumber[i] = tmp.get(i);
			}


			for (int num : diagramNumber) {
				BusStopData start = null;
				BusStopData goal = null;

				datCursor = db.rawQuery("SELECT BusRoute.BusID, Diagram.Time, BusRoute.BusID FROM Diagram " +
						"INNER JOIN BusRoute ON (Diagram.RouteID = BusRoute.RouteID AND Diagram.Number = BusRoute.Number) " +
						"WHERE Diagram.RouteID = ? AND BusRoute.BusID = ? AND Diagram.DiaNumber = ?",
						new String[] { String.valueOf(route.getRouteId()),String.valueOf(startId), String.valueOf(num) });

				if (datCursor.moveToNext()) {
					start = new BusStopData(datCursor.getInt(1), datCursor.getInt(0), datCursor.getInt(2));
				}

				datCursor = db.rawQuery("SELECT BusRoute.BusID, Diagram.Time, BusRoute.BusID FROM Diagram " +
						"INNER JOIN BusRoute ON (Diagram.RouteID = BusRoute.RouteID AND Diagram.Number = BusRoute.Number) " +
						"WHERE Diagram.RouteID = ? AND BusRoute.BusID = ? AND DiaNumber = ?",
						new String[] { String.valueOf(route.getRouteId()), String.valueOf(goalId), String.valueOf(num) });

				if (datCursor.moveToNext()) {
					goal = new BusStopData(datCursor.getInt(1), datCursor.getInt(0), 0);
				}

				if (start != null && goal != null) {
					result.add(new BusPlan(route.getRouteId(), start, goal));
				}
			}

		} catch (SQLException e) {
		} finally {
			if (datCursor != null) { datCursor.close(); }
		}

		return result.toArray(new BusPlan[result.size()]);
	}

	public class BusStopData {
		private int busStopID;
		//private int busStopNumber;
		private BusDate busTime;

		public BusStopData(String str, int busStopID, int busStopNumber) {
			this.busTime = new BusDate(str);

			this.busStopID = busStopID;
			//this.busStopNumber = busStopNumber;
		}

		public BusStopData(int date, int busStopID, int busStopNumber) {
			this.busTime = new BusDate(date);

			this.busStopID = busStopID;
			//this.busStopNumber = busStopNumber;
		}

		public int getBusStopID() {
			return busStopID;
		}

		public BusDate getBusTime() {
			return busTime;
		}
	}

	public class BusTimeData {
		private int routeID;
		private int stopNumber;
		private int busID;
		private int week;
		private ArrayList<BusDate> busTimes;

		public BusTimeData(int routeID, int stopNumber, int week, BusDate[] times) {
			this.routeID = routeID;
			this.week = week;
			this.stopNumber = stopNumber;

			this.busTimes = new ArrayList<BusDate>();
			this.busTimes.addAll(Arrays.asList(times));
		}

		public int getRouteID() {
			return routeID;
		}

		public void addBusTime(BusDate time) {
			busTimes.add(time);
		}

		public int getBusID() {
			return busID;
		}

		public int getStopNumber() {
			return stopNumber;
		}

		public int getWeek() {
			return week;
		}

		public BusDate[] getBusTimes() {
			return busTimes.toArray(new BusDate[busTimes.size()]);
		}
	}

	public class BusPlan {
		private int routeId;
		private BusStopData start, goal;
		private BusStopData[] briefStops;

		public BusPlan(int routeId, BusStopData start, BusStopData goal) {
			this(routeId, start, goal, null);
		}

		public BusPlan(int routeId, BusStopData start, BusStopData goal, BusStopData[] briefStops) {
			this.routeId = routeId;
			this.start = start;
			this.goal = goal;
			this.briefStops = briefStops;
		}

		public BusStopData getStart() {
			return start;
		}

		public int getRouteId() {
			return routeId;
		}

		public BusStopData getGoal() {
			return goal;
		}

		public BusStopData[] getBriefStops() {
			return briefStops;
		}
	}
}
