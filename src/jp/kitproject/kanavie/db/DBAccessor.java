package jp.kitproject.kanavie.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public final class DBAccessor extends SQLiteOpenHelper {

	private static final String DB_NAME = "appdata.db";
	private static final int DB_VERSION = 1;

	private static DBAccessor instance = null;

	public static DBAccessor getInstance(Context context) {
		return (instance == null) ? createInstance(context) : instance;
	}

	private static synchronized DBAccessor createInstance(Context context) {
		return (instance == null) ? instance = new DBAccessor(context, DB_NAME, null, DB_VERSION) : instance;
	}

	private DBAccessor(Context context, String name, CursorFactory factory, int version) {
		super(context, name, factory, version);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		db.execSQL("CREATE TABLE IF NOT EXISTS Landmark(ID INTEGER NOT NULL UNIQUE PRIMARY KEY, StdID INTEGER NOT NULL, Name TEXT NOT NULL, Detail TEXT NOT NULL, Latitude TEXT NOT NULL, Longtitude TEXT NOT NULL)");
		db.execSQL("CREATE TABLE IF NOT EXISTS BusStop(ID INTEGER NOT NULL UNIQUE PRIMARY KEY, Name TEXT NOT NULL, Company TEXT NOT NULL, Latitude TEXT NOT NULL, Longtitude TEXT NOT NULL)");
		db.execSQL("CREATE TABLE IF NOT EXISTS RouteName(RouteID INTEGER NOT NULL PRIMARY KEY, Name TEXT NOT NULL)");
		db.execSQL("CREATE TABLE IF NOT EXISTS BusRoute(RouteID INTEGER NOT NULL, Number INTEGER NOT NULL, BusID INTEGER NOT NULL)");
		db.execSQL("CREATE TABLE IF NOT EXISTS Diagram(RouteID INTEGER NOT NULL, Number INTEGER NOT NULL, Week INTEGER NOT NULL, DiaNumber INTEGER NOT NULL, Time INTEGER NOT NULL)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

	}

}
