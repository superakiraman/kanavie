package jp.kitproject.kanavie.db;

import java.util.ArrayList;

import jp.kitproject.kanavie.Landmark;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

public class LandmarkHolder {

	private static final String TABLE_NAME = "Landmark";

	private final DBAccessor dbConnection;

	public LandmarkHolder(Context context) {
		dbConnection = DBAccessor.getInstance(context);
		dbConnection.getWritableDatabase();
	}

	public boolean initialize(Landmark[] landmark) {
		int complete = 0;
		boolean result = true;

		SQLiteDatabase db = dbConnection.getWritableDatabase();

		try {
			db.beginTransaction();

			db.delete(TABLE_NAME, "", null);

			ContentValues values;
			for (Landmark land : landmark) {
				values = new ContentValues();
				values.put("ID", land.getId());
				values.put("StdID", land.getStandardBusStopId());
				values.put("Name", land.getName());
				values.put("Detail", land.getDetail());
				values.put("Latitude", land.getLocation().getLatitude());
				values.put("Longtitude", land.getLocation().getLongtitude());

				if (db.insert(TABLE_NAME, null, values) > 0) { complete++; }
			}

			db.setTransactionSuccessful();
		} catch (SQLException e) {
			result = false;
		} finally {
			db.endTransaction();
		}

		return result && (complete == landmark.length);
	}

	public Landmark get(int id) {
		Landmark result = null;

		SQLiteDatabase db = dbConnection.getReadableDatabase();

		Cursor datCursor = null;

		try {
			datCursor = db.query(TABLE_NAME,
					new String[] { "ID", "StdID", "Name", "Detail", "Latitude", "Longtitude" },
					"ID = ?", new String[] { String.valueOf(id) }, null, null, null);

			if (datCursor.moveToFirst()) {
				result = new Landmark(datCursor.getInt(0), datCursor.getInt(1),
						datCursor.getString(2), datCursor.getString(3), datCursor.getDouble(4), datCursor.getDouble(5));
			}
		} catch (SQLException e) {
		} finally {
			if (datCursor != null) { datCursor.close(); }
		}

		return result;
	}

	public Landmark get(String name) {
		Landmark result = null;

		SQLiteDatabase db = dbConnection.getReadableDatabase();

		Cursor datCursor = null;

		try {
			datCursor = db.query(TABLE_NAME,
					new String[] { "ID", "StdID", "Name", "Detail", "Latitude", "Longtitude" },
					"Name = ?", new String[] { name }, null, null, null);

			if (datCursor.moveToFirst()) {
				result = new Landmark(datCursor.getInt(0), datCursor.getInt(1),
						datCursor.getString(2), datCursor.getString(3), datCursor.getDouble(4), datCursor.getDouble(5));
			}
		} catch (SQLException e) {
		} finally {
			if (datCursor != null) { datCursor.close(); }
		}

		return result;
	}

	public Landmark[] getAll() {
		ArrayList<Landmark> result = new ArrayList<Landmark>();

		SQLiteDatabase db = dbConnection.getReadableDatabase();

		Cursor datCursor = null;

		try {
			datCursor = db.query(TABLE_NAME,
					new String[] { "ID", "StdID", "Name", "Detail", "Latitude", "Longtitude" },
					null, null, null, null,  "ID asc");

			while (datCursor.moveToNext()) {
				result.add(new Landmark(datCursor.getInt(0), datCursor.getInt(1),
						datCursor.getString(2), datCursor.getString(3), datCursor.getDouble(4), datCursor.getDouble(5)));
			}
		} catch (SQLException e) {
		} finally {
			if (datCursor != null) { datCursor.close(); }
		}

		return result.toArray(new Landmark[result.size()]);
	}
}
