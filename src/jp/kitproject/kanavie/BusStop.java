package jp.kitproject.kanavie;

import java.io.Serializable;

public class BusStop extends Spot implements Serializable {

	private static final long serialVersionUID = 7873684299045773402L;

	int id;
	String name;
	String company;
	EarthLocation location;

	public BusStop(int id, String name, String company, EarthLocation location) {
		this.id = id;
		this.name = name;
		this.company = company;
		this.location = location;
	}

	public BusStop(int id, String name, String company, double latitude, double longtitude) {
		this.id = id;
		this.name = name;
		this.company = company;
		this.location = new EarthLocation(latitude, longtitude);
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getCompany() {
		return company;
	}

	public EarthLocation getLocation() {
		return location;
	}

	@Override
	public int getStandardBusStopId() {
		return id;
	}
}
