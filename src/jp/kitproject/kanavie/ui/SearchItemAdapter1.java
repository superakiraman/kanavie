package jp.kitproject.kanavie.ui;

import jp.kitproject.kanavie.ImageProvider;
import jp.kitproject.kanavie.Landmark;
import jp.kitproject.kanavie.LandmarkDiff;
import jp.kitproject.kanavie.R;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


public class SearchItemAdapter1 extends BaseAdapter {

	private LayoutInflater layoutInflater = null;
	private View[] searchResultViewItems = null;
	private Landmark[] landmarks = null;

	public SearchItemAdapter1(Context context, final Landmark[] landmark) {
		landmarks = landmark;
		searchResultViewItems = new View[landmark.length];

		//LayoutInflaterを取得
        layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        for (int i = 0; i < landmark.length; i++) {
        	View view = layoutInflater.inflate(R.layout.search_item1_inflater, null);

        	TextView titleTextView = (TextView)view.findViewById(R.id.search_item_title_textview);
        	titleTextView.setText(landmark[i].getName());

        	TextView detailTextView = (TextView)view.findViewById(R.id.search_item_detail_textview);
        	detailTextView.setText(landmark[i].getDetail());

        	ImageView imageView = (ImageView)view.findViewById(R.id.search_item_imageview);
        	AsyncImageSetter ais = new AsyncImageSetter(imageView);
        	ais.execute(landmark[i].getId());

        	searchResultViewItems[i] = view;
        }
	}

	public SearchItemAdapter1(Context context, final LandmarkDiff[] landmark) {
		landmarks = landmark;
		searchResultViewItems = new View[landmark.length];

		//LayoutInflaterを取得
        layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        for (int i = 0; i < landmark.length; i++) {
        	View view = layoutInflater.inflate(R.layout.search_item1_inflater, null);

        	int d = (int)(Math.round(landmark[i].getDistance() / 100) * 100);

        	TextView titleTextView = (TextView)view.findViewById(R.id.search_item_title_textview);
        	titleTextView.setText(d + "m\n" + landmark[i].getName());

        	TextView detailTextView = (TextView)view.findViewById(R.id.search_item_detail_textview);
        	detailTextView.setText(landmark[i].getDetail());

        	ImageView imageView = (ImageView)view.findViewById(R.id.search_item_imageview);
        	AsyncImageSetter ais = new AsyncImageSetter(imageView);
        	ais.execute(landmark[i].getId());

        	searchResultViewItems[i] = view;
        }
	}

	@Override
	public int getCount() {
		return searchResultViewItems.length;
	}

	@Override
	public Object getItem(int position) {
		return landmarks[position];
	}

	@Override
	public long getItemId(int position) {
		return landmarks[position].getId();
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return searchResultViewItems[position];
	}

	public class AsyncImageSetter extends AsyncTask<Integer, Void, Drawable> {

		ImageView imageView = null;

		public AsyncImageSetter(ImageView imageView) {
			this.imageView = imageView;
		}

		@Override
		protected Drawable doInBackground(Integer... params) {
			return ImageProvider.getImage(params[0]);
		}

		@Override
		protected void onPostExecute(Drawable result) {
			if (result != null) {
				imageView.setImageDrawable(result);
			}
		}

	}

}
