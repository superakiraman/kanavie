package jp.kitproject.kanavie;

import android.content.Context;
import jp.kitproject.kanavie.db.BusRouteHolder;
import jp.kitproject.kanavie.db.BusRouteHolder.BusRoute;
import jp.kitproject.kanavie.db.BusStopHolder;


public final class LocationUtilities {

	public static double degreeToRadian(double degree) {
		return degree * Math.PI / 180;
	}

	public static double calcDistanceDifference(double alat, double alng, double blat, double blng) {
		return calcDistanceDifference(new EarthLocation(alat, alng), new EarthLocation(blat, blng));
	}

	public static double calcDistanceDifference(EarthLocation a, EarthLocation b) {
		// 厳密には計算しない
		double dy = degreeToRadian((a.getLatitude() - b.getLatitude())) * 6378137;
		double dx = degreeToRadian((a.getLongtitude() - b.getLongtitude())) * 6378137 * Math.cos(degreeToRadian(a.getLatitude()));

		return Math.sqrt(dy * dy + dx * dx);
	}

	public static BusStop searchNearestBusStop(Context context, EarthLocation loc) {
		BusStop[] busstops = new BusStopHolder(context).getAll();

		BusStop nearBusstop = busstops[0];
		double distance = LocationUtilities.calcDistanceDifference(loc, nearBusstop.getLocation());
		for (BusStop bs : busstops) {
			double d = LocationUtilities.calcDistanceDifference(loc, bs.getLocation());
			if (d < distance) {
				distance = d;
				nearBusstop = bs;
			}
		}

		return nearBusstop;

	}

	public static void searchBusRoute(Context context, BusStop start, BusStop goal) {
		BusRouteHolder brh = new BusRouteHolder(context);
		BusRoute[] route = brh.get(start.getId(), goal.getId());

	}
}
