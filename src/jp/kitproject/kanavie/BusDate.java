package jp.kitproject.kanavie;

import java.util.Date;


public class BusDate {
	private int date = 0;

	public BusDate() {
		Date d = new Date();
		date = d.getHours() * 60 + d.getMinutes();
	}

	public BusDate(String str) {
		String[] s = str.split(":");
		int hour = Integer.parseInt(s[0]);
		int minute = Integer.parseInt(s[1]);

		date = hour * 60 + minute;
	}

	public BusDate(int date) {
		this.date = date;
	}

	public int getDate() {
		return date;
	}

	public int getHour() {
		return date / 60;
	}

	public int getMinute() {
		return date % 60;
	}

	@Override
	public String toString() {
		int hour = date / 60;
		int minute = date % 60;

		return String.format("%02d", hour) + ":" + String.format("%02d", minute);
	}
}
