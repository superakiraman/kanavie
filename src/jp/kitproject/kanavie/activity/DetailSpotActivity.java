package jp.kitproject.kanavie.activity;

import jp.kitproject.kanavie.ImageProvider;
import jp.kitproject.kanavie.Landmark;
import jp.kitproject.kanavie.R;
import jp.kitproject.kanavie.network.HttpUtilities;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class DetailSpotActivity extends Activity {

	private int spotId = 0;
	private Context appContext = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail);

        appContext = getApplicationContext();

        Landmark landmark = null;

        Bundle extra = getIntent().getExtras();
        if (extra == null) { return; }

        landmark = (Landmark)extra.getSerializable("location");
        spotId = landmark.getId();

        TextView placeName = (TextView)findViewById(R.id.detail_title_textview);
        placeName.setText(landmark.getName());

        String s = HttpUtilities.getString("http://kfc9.usamimi.info/kanavie/landmark/" + spotId + ".txt", "Shift-JIS");

        TextView placeDetail = (TextView)findViewById(R.id.detail_detail_textview);
        placeDetail.setText(s != null ? s : "読み込みに失敗しました");

        ImageView imageView = (ImageView)findViewById(R.id.detail_place_imageview);

        Drawable img = ImageProvider.getImage(landmark.getId());
        if (img != null) { imageView.setImageDrawable(img); }

        Button setSpot = (Button)findViewById(R.id.detail_set_button);
        setSpot.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(appContext, SearchRouteActivity.class);
				intent.putExtra("goalID", spotId);
				startActivity(intent);
			}
		});

    }
}
