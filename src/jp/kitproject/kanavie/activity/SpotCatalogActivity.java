package jp.kitproject.kanavie.activity;

import java.util.ArrayList;

import jp.kitproject.kanavie.BusStop;
import jp.kitproject.kanavie.Landmark;
import jp.kitproject.kanavie.R;
import jp.kitproject.kanavie.db.BusStopHolder;
import jp.kitproject.kanavie.db.LandmarkHolder;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;


public class SpotCatalogActivity extends Activity {

	public final static int MODE_TOP = 0;
	public final static int MODE_LANDMARK = 1;
	public final static int MODE_BUSSTOP = 2;

	private SpotCatalogActivity myActivity = null;

	private Context appContext = null;

	private int mode = 0;
	private boolean showLocation = true;

	private ArrayList<Integer> idList = null;

	private ListView spotCatalogListView = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.spot_catalog);

		myActivity = this;
		appContext = this.getApplicationContext();
//		selfhandler = new Handler();

		idList = new ArrayList<Integer>();

		Bundle extra = getIntent().getExtras();
		if (extra != null) {
			mode = extra.getInt("mode", 0);
			showLocation = extra.getBoolean("showLocation", true);
		}

		spotCatalogListView = (ListView)findViewById(R.id.spot_catalog_listview);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1);

		if (mode == 0) {
			if (showLocation) {
				adapter.add("現在地");
			}
			adapter.add("観光地");
			adapter.add("バス停");
		} else if (mode == 1) {
			LandmarkHolder lh  = new LandmarkHolder(appContext);

			for (Landmark lm : lh.getAll()) {
				idList.add(lm.getId());
				adapter.add(lm.getName());
			}
		} else if (mode == 2) {
			BusStopHolder bsh = new BusStopHolder(appContext);

			for (BusStop bs : bsh.getAll()) {
				idList.add(bs.getId());
				adapter.add(bs.getName() + "(" + bs.getCompany() + ")");
			}
		}

		spotCatalogListView.setAdapter(adapter);

		spotCatalogListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
				if (mode == MODE_TOP) {
					if (!showLocation) { arg2++; }

					switch(arg2) {
						case 0:
							setResult(RESULT_OK, getIntent().putExtra("useGps", true));

							finish();
							break;
						case MODE_LANDMARK:
						case MODE_BUSSTOP:
							Intent intent = new Intent(appContext, SpotCatalogActivity.class);
							intent.putExtra("mode", arg2);
							startActivityForResult(intent, 0);
							break;
					}
				} else {
					setResult(RESULT_OK, getIntent().putExtra("spot_id", idList.get(arg2)));

					finish();
				}

			}
		});
	}

	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK && requestCode == 0){
        	setResult(RESULT_OK, getIntent().putExtra("spot_id", data.getIntExtra("spot_id", 0)));

        	finish();
        }
    }
}
