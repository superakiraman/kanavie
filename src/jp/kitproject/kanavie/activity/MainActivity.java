package jp.kitproject.kanavie.activity;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import jp.kitproject.kanavie.BusDate;
import jp.kitproject.kanavie.BusStop;
import jp.kitproject.kanavie.Landmark;
import jp.kitproject.kanavie.R;
import jp.kitproject.kanavie.db.BusRouteHolder;
import jp.kitproject.kanavie.db.BusRouteHolder.BusRoute;
import jp.kitproject.kanavie.db.BusStopHolder;
import jp.kitproject.kanavie.db.DiagramHolder;
import jp.kitproject.kanavie.db.LandmarkHolder;
import jp.kitproject.kanavie.db.DiagramHolder.BusTimeData;
import jp.kitproject.kanavie.network.HttpUtilities;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;

public class MainActivity extends Activity {

	private Context appContext = null;
	private ProgressDialog progressDialog = null;

	private Handler selfhandler = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		appContext = this.getApplicationContext();
		selfhandler = new Handler();

		SharedPreferences dataPref =
	        	getSharedPreferences("UpdateSetting", Context.MODE_PRIVATE);

		// イベントリスナーの設定
		ImageButton button = (ImageButton) findViewById(R.id.imageButton1);
		button.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(appContext, SearchSpotActivity.class);
				i.putExtra("location", false);
				startActivity(i);
			}
		});

		ImageButton button2 = (ImageButton) findViewById(R.id.imageButton2);
		button2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(appContext, SearchSpotActivity.class);
				i.putExtra("location", true);
				startActivity(i);
			}
		});

		ImageButton button3 = (ImageButton) findViewById(R.id.imageButton3);
		button3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent i = new Intent(appContext, SearchRouteActivity.class);
				startActivity(i);
			}
		});

		boolean gotData = dataPref.getBoolean("gotData", false);

		int gotDate = dataPref.getInt("gotDate", 0);
		int diffDate = gotDate - (int)(new Date().getTime() / 1000 / 60 / 60 / 24);

		if (!gotData || diffDate > 7) {
			new AlertDialog.Builder(this)
			.setMessage("データをダウンロードしますか")
			.setCancelable(true)
			.setPositiveButton("OK", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					initilizeData();
				}
			})
			.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}
			})
			.show();
		}
	}

	// オプションメニューが最初に呼び出される時に1度だけ呼び出されます
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	MenuInflater inflater = getMenuInflater();
    	inflater.inflate(R.menu.main_activity_menu, menu);

        return super.onCreateOptionsMenu(menu);
    }

	// オプションメニューが表示される度に呼び出されます
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        return super.onPrepareOptionsMenu(menu);
    }

    // オプションメニューアイテムが選択された時に呼び出されます
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        boolean ret = true;

        switch (item.getItemId()) {
        	default:
        		ret = super.onOptionsItemSelected(item);
        		break;

        	// 定義の追加
        	case R.id.main_menu_refrash:
        		new AlertDialog.Builder(this)
    			.setMessage("データをダウンロードしますか")
    			.setCancelable(true)
    			.setPositiveButton("OK", new DialogInterface.OnClickListener() {
    				public void onClick(DialogInterface dialog, int id) {
    					initilizeData();
    				}
    			})
    			.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
    				@Override
    				public void onClick(DialogInterface dialog, int which) {
    					dialog.dismiss();
    				}
    			})
    			.show();
        		break;

        	// About画面
        	case R.id.main_menu_about:

        		break;

        }

        return ret;
    }

	private void initilizeData() {
		progressDialog = new ProgressDialog(this);
		progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progressDialog.setCancelable(false);
		progressDialog.setTitle("処理中");
		progressDialog.setMessage("データをダウンロード中です");

		progressDialog.show();

		new Thread(new Runnable() {

			@Override
			public void run() {
				String s = HttpUtilities.getString("http://kfc9.usamimi.info/kanavie/landmark.csv", "Shift-JIS");
				//String s = HttpUtilities.getString("http://192.168.24.31:8084/test.csv", "Shift-JIS");

				ArrayList<Landmark> landmarks = new ArrayList<Landmark>();
				String[] data = s.replace("\r", "").split("\n");
				for (String dat : data) {
					String[] sub = dat.split(",");
					landmarks.add(new Landmark(Integer.parseInt(sub[0]), Integer.parseInt(sub[5]),
							sub[1], sub[2], Double.parseDouble(sub[3]), Double.parseDouble(sub[4])));
				}

				LandmarkHolder lh = new LandmarkHolder(appContext);
				lh.initialize(landmarks.toArray(new Landmark[landmarks.size()]));

				s = HttpUtilities.getString("http://kfc9.usamimi.info/kanavie/busstop.csv", "Shift-JIS");
				//s = HttpUtilities.getString("http://192.168.24.31:8084/busstop.csv", "Shift-JIS");

				ArrayList<BusStop> busstops = new ArrayList<BusStop>();
				data = s.replace("\r", "").split("\n");
				for (String dat : data) {
					String[] sub = dat.split(",");
					busstops.add(new BusStop(Integer.parseInt(sub[0]), sub[1], sub[4], Double.parseDouble(sub[2]), Double.parseDouble(sub[3])));
				}

				BusStopHolder bh = new BusStopHolder(appContext);
				bh.initialize(busstops.toArray(new BusStop[busstops.size()]));

				BusRouteHolder brh = new BusRouteHolder(appContext);
				ArrayList<BusRoute> br = new ArrayList<BusRouteHolder.BusRoute>();

				s = HttpUtilities.getString("http://kfc9.usamimi.info/kanavie/busroute.csv", "Shift-JIS");
				data = s.replace("\r", "").split("\n");
				for (String dat : data) {
					String[] sub = dat.split(",");

					int[] stop = new int[sub.length - 2];
					for (int i = 2; i < sub.length; i++) {
						stop[i - 2] = Integer.parseInt(sub[i]);
					}

					br.add(brh.new BusRoute(Integer.parseInt(sub[0]), stop, sub[1]));
				}

				brh.initialize(br.toArray(new BusRoute[br.size()]));

				s = HttpUtilities.getString("http://kfc9.usamimi.info/kanavie/bustime.csv", "Shift-JIS");
				data = s.replace("\r", "").split("\n");

				DiagramHolder bth = new DiagramHolder(appContext);
				ArrayList<BusTimeData> data2 = new ArrayList<BusTimeData>();
				for (String dat : data) {
					String[] sub = dat.split(",");
					ArrayList<BusDate> bd = new ArrayList<BusDate>();
					for (int i = 3; i < sub.length; i++) {
						if (sub[i].length() > 3) {
							bd.add(new BusDate(sub[i]));
						}
					}

					data2.add(bth.new BusTimeData(
							Integer.parseInt(sub[0]),
							Integer.parseInt(sub[1]),
							Integer.parseInt(sub[2]),
							bd.toArray(new BusDate[0])));
				}

				bth.initialize(data2.toArray(new BusTimeData[data2.size()]));

				progressDialog.dismiss();

				selfhandler.post(new Runnable() {
					@Override
					public void run() {
						completeInitilizeData();
					}
				});
			}
		}).start();
	}

	private void completeInitilizeData() {
		new AlertDialog.Builder(this)
		.setMessage("データのダウンロードが完了しました")
		.setCancelable(true)
		.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.dismiss();
			}
		})
		.show();

		SharedPreferences dataPref =
	        	getSharedPreferences("UpdateSetting", Context.MODE_PRIVATE);

		dataPref.edit().putBoolean("gotData", true).putInt("gotDate", (int)(new Date().getTime() / 1000 / 60 / 60 / 24)).commit();
	}
}
