package jp.kitproject.kanavie.activity;

import java.util.Calendar;

import jp.kitproject.kanavie.BusDate;
import jp.kitproject.kanavie.BusStop;
import jp.kitproject.kanavie.EarthLocation;
import jp.kitproject.kanavie.Landmark;
import jp.kitproject.kanavie.LocationUtilities;
import jp.kitproject.kanavie.R;
import jp.kitproject.kanavie.Spot;
import jp.kitproject.kanavie.db.BusStopHolder;
import jp.kitproject.kanavie.db.DiagramHolder;
import jp.kitproject.kanavie.db.LandmarkHolder;
import android.app.Activity;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TimePicker;

public class SearchRouteActivity extends Activity {

	private SearchRouteActivity myActivity = null;

	private Context appContext = null;
	private ProgressDialog progressDialog = null;

	private boolean useLocation = false;
	private EarthLocation gpsLocation = null;

	private BusDate baseTime = null;

	private Spot goalSpot = null;
	private Spot startSpot = null;

	private LocationListener llisner = null;
	private LocationManager locMan = null;

	private Button startSpotButton = null;
	private Button goalSpotButton = null;
	private Button setBaseTimeButton = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search_route);

		myActivity = this;
		appContext = getApplicationContext();
//		selfhandler = new Handler();

		locMan = (LocationManager)appContext.getSystemService(Context.LOCATION_SERVICE);

		Bundle extra = getIntent().getExtras();
		if (extra != null) {
			startSpot = getSpotById(extra.getInt("startID", 0));
			goalSpot = getSpotById(extra.getInt("goalID", 0));
		}

		baseTime = new BusDate();

		startSpotButton = (Button)findViewById(R.id.search_route_start_button);
		goalSpotButton = (Button)findViewById(R.id.search_route_goal_button);
		setBaseTimeButton = (Button)findViewById(R.id.search_route_time_button);

		startSpotButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(appContext, SpotCatalogActivity.class);
				startActivityForResult(intent, 1);
			}
		});

		goalSpotButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(appContext, SpotCatalogActivity.class);
				intent.putExtra("showLocation", false);
				startActivityForResult(intent, 2);
			}
		});

		setBaseTimeButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
		        new TimePickerDialog(myActivity, new TimePickerDialog.OnTimeSetListener() {
					@Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
						baseTime = new BusDate(hourOfDay * 60 + minute);
						setBaseTimeButton.setText(baseTime.toString());
		        	}
				}, baseTime.getHour(), baseTime.getMinute(), true)
				.show();
			}
		});

		Button searchRouteButton = (Button)findViewById(R.id.search_route_search_button);
		searchRouteButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (startSpot != goalSpot &&
						(startSpot != null || useLocation) && goalSpot != null) {
					if (useLocation) {
						startSearchLocation();
					} else {
						searchRoute();
					}
				}
			}
		});

		progressDialog = new ProgressDialog(this);
		progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
		progressDialog.setTitle("処理中");
		progressDialog.setMessage("位置情報を取得中");
		progressDialog.setOnCancelListener(new OnCancelListener() {
			@Override
			public void onCancel(DialogInterface dialog) {
				progressDialog.cancel();
				locMan.removeUpdates(llisner);
			}
		});

		updateButtonText();
	}

	@Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK){
        	switch(requestCode) {
        		case 1:
        			startSpot = getSpotById(data.getIntExtra("spot_id", 0));
        			useLocation = data.getBooleanExtra("useGps", false);
        			break;
        		case 2:
        			goalSpot = getSpotById(data.getIntExtra("spot_id", 0));
        			break;
        	}

        	updateButtonText();
        }
    }

	private Spot getSpotById(int id) {
		if (id == 0) {
			return null;
		} else if (id < 1000) {
			LandmarkHolder lh = new LandmarkHolder(appContext);
			return lh.get(id);
		} else {
			BusStopHolder bsh = new BusStopHolder(appContext);
			return bsh.get(id);
		}
	}

	private void updateButtonText() {
		startSpotButton.setText(startSpot != null ? startSpot.getName() : useLocation ? "現在地" : "出発地点を選択");
		goalSpotButton.setText(goalSpot != null ? goalSpot.getName() : "到着地点を選択");
	}

	public void startSearchLocation() {
		progressDialog.show();

		Criteria criteria = new Criteria();
		criteria.setSpeedRequired(false);
		criteria.setAltitudeRequired(false);
		criteria.setBearingRequired(false);

		String provider = locMan.getBestProvider(criteria, true);

		llisner = new LocationListener() {
			@Override
			public void onStatusChanged(String provider, int status,
					Bundle extras) {
			}

			@Override
			public void onProviderEnabled(String provider) {
			}

			@Override
			public void onProviderDisabled(String provider) {
			}

			@Override
			public void onLocationChanged(Location location) {
				locMan.removeUpdates(llisner);

				gpsLocation = new EarthLocation(location.getLatitude(), location.getLongitude());
				progressDialog.dismiss();

				searchRoute();
			}
		};

		locMan.requestLocationUpdates(provider, 0, 0, llisner);
	}

	public void searchRoute() {
		Intent intent = new Intent(appContext, RouteResultActivity.class);
		intent.putExtra("startSpot", startSpot);
		intent.putExtra("goalSpot", goalSpot);
		intent.putExtra("bTime", baseTime.getDate());
		intent.putExtra("useGps", useLocation);
		intent.putExtra("gpsLocation", gpsLocation);
		startActivity(intent);
	}
}
