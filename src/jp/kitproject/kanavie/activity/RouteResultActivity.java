package jp.kitproject.kanavie.activity;

import java.util.ArrayList;

import jp.kitproject.kanavie.BusDate;
import jp.kitproject.kanavie.BusStop;
import jp.kitproject.kanavie.EarthLocation;
import jp.kitproject.kanavie.LocationUtilities;
import jp.kitproject.kanavie.R;
import jp.kitproject.kanavie.Spot;
import jp.kitproject.kanavie.db.BusRouteHolder;
import jp.kitproject.kanavie.db.BusStopHolder;
import jp.kitproject.kanavie.db.DiagramHolder;
import jp.kitproject.kanavie.db.DiagramHolder.BusPlan;
import jp.kitproject.kanavie.db.LandmarkHolder;
import jp.kitproject.kanavie.db.BusRouteHolder.BusRoute;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;


public class RouteResultActivity extends Activity {

	private Context appContext = null;

	private boolean useLocation = false;
	private EarthLocation gpsLocation = null;

	private Spot goalSpot = null;
	private Spot startSpot = null;

	private int baseTime = -1;

	BusStop nearestBusStart = null;
	BusStop nearestBusGoal = null;

	BusPlan[] plans = null;

	TextView tv1, tv2, tv3, tv4, tv2time, tv3time, tv2Name;
	ImageView im1, im2, im3;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search_result);

		appContext = this.getApplicationContext();

		Bundle extra = getIntent().getExtras();
		if (extra != null) {
			startSpot = (Spot)extra.getSerializable("startSpot");
			goalSpot = (Spot)extra.getSerializable("goalSpot");

			baseTime = extra.getInt("bTime", new BusDate().getDate());

			useLocation = extra.getBoolean("useGps", false);
			if (useLocation) { gpsLocation = (EarthLocation)extra.getSerializable("gpsLocation"); }
		}

		if (startSpot == goalSpot ||
				(startSpot == null && !useLocation) || goalSpot == null) {
			finish();
		}

		tv1 = (TextView)findViewById(R.id.search_result_start);
		tv2 = (TextView)findViewById(R.id.search_result_start_bus);
		tv3 = (TextView)findViewById(R.id.search_result_goal_bus);
		tv4 = (TextView)findViewById(R.id.search_result_goal);
		tv2time = (TextView)findViewById(R.id.search_result_start_bus_time);
		tv3time = (TextView)findViewById(R.id.search_result_goal_bus_time);
		tv2Name = (TextView)findViewById(R.id.search_result_ride_bus_name);

		im1 = (ImageView)findViewById(R.id.search_result_arrow1);
		im1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (useLocation) {
					callMap(gpsLocation, nearestBusStart.getLocation());
				} else {
					callMap(startSpot.getLocation(), nearestBusStart.getLocation());
				}
			}
		});
		im2 = (ImageView)findViewById(R.id.search_result_arrow2);
		im2.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				callMap(nearestBusStart.getLocation(), nearestBusGoal.getLocation());
			}
		});
		im3 = (ImageView)findViewById(R.id.search_result_arrow3);
		im3.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				callMap(nearestBusGoal.getLocation(), goalSpot.getLocation());
			}
		});

		Button showMap = (Button)findViewById(R.id.search_result_show_map);
		showMap.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				if (useLocation) {
					callMap(gpsLocation, goalSpot.getLocation());
				} else {
					callMap(startSpot.getLocation(), goalSpot.getLocation());
				}
			}
		});

		BusStopHolder bsh = new BusStopHolder(appContext);

		if (useLocation) {
			nearestBusStart = LocationUtilities.searchNearestBusStop(appContext, gpsLocation);
		} else {
			//nearestBusStart = LocationUtilities.searchNearestBusStop(appContext, startSpot.getLocation());
			nearestBusStart = bsh.get(startSpot.getStandardBusStopId());
		}
		//nearestBusGoal = LocationUtilities.searchNearestBusStop(appContext, goalSpot.getLocation());
		nearestBusGoal = bsh.get(goalSpot.getStandardBusStopId());

		BusRouteHolder brh = new BusRouteHolder(appContext);
		BusRoute[] route = brh.get(nearestBusStart.getId(), nearestBusGoal.getId());
		//BusRoute[] route = brh.get(1033, 1032);

		BusPlan mainPlan = null;

		int timeDiff = 2000;
		DiagramHolder dh = new DiagramHolder(appContext);
		for (BusRoute rt : route) {
			plans = dh.get(nearestBusStart.getId(), nearestBusGoal.getId(), rt, new BusDate(baseTime));
			if (plans.length == 0) { continue; }

			int d = plans[0].getStart().getBusTime().getDate() - baseTime;
			if (plans.length > 0 && timeDiff > d) {
				timeDiff = d;
				mainPlan = plans[0];
			}
		}

		if (mainPlan != null) {
			showResult(mainPlan);
		} else {
			View v = findViewById(R.id.search_result_notfound);
			v.setVisibility(View.VISIBLE);
		}

	}

	private void showResult(BusPlan plan) {
		if (useLocation) {
			tv1.setText("現在地");
		} else {
			tv1.setText(startSpot.getName());
		}
		tv2.setText(nearestBusStart.getCompany() + " - " + nearestBusStart.getName());
		tv3.setText(nearestBusGoal.getCompany() + " - " + nearestBusGoal.getName());
		tv4.setText(goalSpot.getName());

		tv2time.setText(plan.getStart().getBusTime().toString());
		tv3time.setText(plan.getGoal().getBusTime().toString());

		BusRouteHolder brh = new BusRouteHolder(appContext);
		tv2Name.setText(brh.get(plan.getRouteId()).getRouteName());

		View v = findViewById(R.id.search_result_found);
		v.setVisibility(View.VISIBLE);
	}

	private void callMap(EarthLocation location1, EarthLocation location2) {
		Intent intent = new Intent();
		intent.setAction(Intent.ACTION_VIEW);
		intent.setClassName("com.google.android.apps.maps","com.google.android.maps.MapsActivity");
		intent.setData(Uri.parse("http://maps.google.com/maps?saddr=" + location1.toString() + "&daddr=" + location2.toString() + "&dirflg=w"));
		startActivity(intent);
	}

	private Spot getSpotById(int id) {
		if (id == 0) {
			return null;
		} else if (id < 1000) {
			LandmarkHolder lh = new LandmarkHolder(appContext);
			return lh.get(id);
		} else {
			BusStopHolder bsh = new BusStopHolder(appContext);
			return bsh.get(id);
		}
	}

}
