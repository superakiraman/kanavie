package jp.kitproject.kanavie.activity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

import java.util.Map;


import jp.kitproject.kanavie.EarthLocation;
import jp.kitproject.kanavie.Landmark;
import jp.kitproject.kanavie.LandmarkDiff;
import jp.kitproject.kanavie.LocationUtilities;
import jp.kitproject.kanavie.R;

import jp.kitproject.kanavie.db.LandmarkHolder;

import jp.kitproject.kanavie.ui.SearchItemAdapter1;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;

import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager.LayoutParams;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;

import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView.OnEditorActionListener;

public class SearchSpotActivity extends Activity {

	private SearchSpotActivity myActivity = null;

	private Context appContext = null;
	private ProgressDialog progressDialog = null;

	private Handler selfhandler = null;

	private Landmark[] landmarks = null;

	private TextView searchModeTextView = null;
	private LocationListener llisner = null;

	private LocationManager locMan = null;

	private boolean searching = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.getWindow().setSoftInputMode(LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		setContentView(R.layout.search);

		myActivity = this;
		appContext = this.getApplicationContext();
		selfhandler = new Handler();

		locMan = (LocationManager)appContext.getSystemService(Context.LOCATION_SERVICE);

		searchModeTextView = (TextView)findViewById(R.id.search_mode_textview);

		boolean isGetLocation = false;
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
			isGetLocation = extras.getBoolean("location");
		}

		LandmarkHolder lh = new LandmarkHolder(this.getApplicationContext());
		landmarks = lh.getAll();

		SearchItemAdapter1 adapter = new SearchItemAdapter1(this, new Landmark[0]);

		ListView listView = (ListView)findViewById(R.id.listView1);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				Landmark item = (Landmark) ((SearchItemAdapter1) arg0
						.getAdapter()).getItem(arg2);

				Intent intent = new Intent(appContext, DetailSpotActivity.class);
				intent.putExtra("location", item);
				startActivity(intent);
			}
		});

		EditText searchEditText = (EditText)findViewById(R.id.search_search_edittext);
		searchEditText.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_SEARCH) {
					searchLandmark(v.getText().toString());
		        }
		        return true;
			}
		});

		Button searchButton = (Button)findViewById(R.id.search_search_button);
		searchButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				EditText searchEditText = (EditText)findViewById(R.id.search_search_edittext);
				searchLandmark(searchEditText.getText().toString());
			}
		});

		if (isGetLocation) {
			LinearLayout search = (LinearLayout) findViewById(R.id.search_search_layout);
			search.setVisibility(LinearLayout.GONE);

			progressDialog = new ProgressDialog(this);
			progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progressDialog.setTitle("処理中");
			progressDialog.setMessage("位置情報を取得中");
			progressDialog.setOnCancelListener(new OnCancelListener() {
				@Override
				public void onCancel(DialogInterface dialog) {
					progressDialog.cancel();
					locMan.removeUpdates(llisner);
					myActivity.finish();
				}
			});

			progressDialog.show();
			startSearchLocation();
		} else {
			adapter = new SearchItemAdapter1(appContext, landmarks);
			listView.setAdapter(adapter);
		}
	}

	public void startSearchLocation() {
		Criteria criteria = new Criteria();
		criteria.setSpeedRequired(false);
		criteria.setAltitudeRequired(false);
		criteria.setBearingRequired(false);

		String provider = locMan.getBestProvider(criteria, true);

		llisner = new LocationListener() {
			@Override
			public void onStatusChanged(String provider, int status,
					Bundle extras) {
			}

			@Override
			public void onProviderEnabled(String provider) {
			}

			@Override
			public void onProviderDisabled(String provider) {
			}

			@Override
			@SuppressWarnings({ "rawtypes", "unchecked" })
			public void onLocationChanged(Location location) {
				locMan.removeUpdates(llisner);

				EarthLocation eal = new EarthLocation(location.getLatitude(), location.getLongitude());

				LandmarkDiff[] nearLandmarks = new LandmarkDiff[landmarks.length];
				HashMap<LandmarkDiff, Double> diff = new HashMap<LandmarkDiff, Double>();
				for (Landmark lm : landmarks) {
					double d = LocationUtilities.calcDistanceDifference(eal, lm.getLocation());
					diff.put(new LandmarkDiff(lm, d), d);
				}

				// ソートして表示
				ArrayList<Map.Entry> entries = new ArrayList<Map.Entry>(diff.entrySet());
				Collections.sort(entries, new Comparator() {
					public int compare(Object o1, Object o2) {
						Map.Entry e1 = (Map.Entry)o1;
						Map.Entry e2 = (Map.Entry)o2;
						return ((Double)e1.getValue()).compareTo((Double)e2.getValue());
					}
				});

				int i = 0;
				for (Map.Entry entry : entries) {
					nearLandmarks[i++] = (LandmarkDiff)entry.getKey();
				}

				searchModeTextView.setVisibility(View.VISIBLE);
				searchModeTextView.setText("近い順に観光地を表示中");

				SearchItemAdapter1 adapter = new SearchItemAdapter1(appContext, nearLandmarks);
				ListView listView = (ListView)findViewById(R.id.listView1);
				listView.setAdapter(adapter);

				//BusStop nearBusstop = LocationUtilities.searchNearestBusStop(appContext, eal);

				progressDialog.dismiss();
			}
		};

		locMan.requestLocationUpdates(provider, 0, 0, llisner);
	}

	public void searchLandmark(String searchWord) {
		if (!searching && searchWord.length() < 1) { return; }

		SearchItemAdapter1 adapter;
		Button searchButton = (Button)findViewById(R.id.search_search_button);
		EditText searchEditText = (EditText)findViewById(R.id.search_search_edittext);

		if (searching) {
			searching = false;
			searchEditText.setText("");
			searchButton.setText("検索");

			adapter = new SearchItemAdapter1(appContext, landmarks);
		} else {
			searching = true;
			searchButton.setText("クリア");

			// IMEの非表示
			InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(searchEditText.getWindowToken(), 0);

			ArrayList<Landmark> landmark = new ArrayList<Landmark>();
			for (Landmark lm : landmarks) {
				if (lm.getName().contains(searchWord)) {
					landmark.add(lm);
				}
			}

			adapter = new SearchItemAdapter1(appContext, landmark.toArray(new Landmark[landmark.size()]));
		}

		ListView listView = (ListView)findViewById(R.id.listView1);
		listView.setAdapter(adapter);
	}

}
