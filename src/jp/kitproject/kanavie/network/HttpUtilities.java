package jp.kitproject.kanavie.network;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

public class HttpUtilities {

	public static String getString(String url, String code) {
		String result = null;

		try {
			HttpGet method = new HttpGet(url);

			DefaultHttpClient client = new DefaultHttpClient();

			HttpResponse response = client.execute(method);
			int status = response.getStatusLine().getStatusCode();

			if (status == HttpStatus.SC_OK) {
				result = EntityUtils.toString(response.getEntity(), code);
			}
		} catch (Exception e) { }

		return result;
	}

	public static byte[] getBinary(String url) {
		byte[] result = null;

		try {
			HttpGet method = new HttpGet(url);

			DefaultHttpClient client = new DefaultHttpClient();

			HttpResponse response = client.execute(method);
			int status = response.getStatusLine().getStatusCode();

			if (status == HttpStatus.SC_OK) {
				result = EntityUtils.toByteArray(response.getEntity());
			}
		} catch (Exception e) { }

		return result;
	}

}
