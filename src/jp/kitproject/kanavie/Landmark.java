package jp.kitproject.kanavie;

import java.io.Serializable;


public class Landmark extends Spot implements Serializable {

	private static final long serialVersionUID = -3349886310188820895L;

	private int id;
	private int busStopId;
	private String name;
	private String detail;
	private EarthLocation location;

	public Landmark(int id, int busStopId, String name, String detail, EarthLocation location) {
		this.id = id;
		this.busStopId = busStopId;
		this.name = name;
		this.detail = detail;
		this.location = location;
	}

	public Landmark(int id, int busStopId, String name, String detail, double latitude, double longtitude) {
		this(id, busStopId, name, detail, new EarthLocation(latitude, longtitude));
	}


	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getDetail() {
		return detail;
	}

	public EarthLocation getLocation() {
		return location;
	}

	@Override
	public int getStandardBusStopId() {
		return busStopId;
	}
}
