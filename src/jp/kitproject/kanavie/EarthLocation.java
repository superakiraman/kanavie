package jp.kitproject.kanavie;

import java.io.Serializable;


public final class EarthLocation implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -6814971905773978583L;

	private double latitude;
	private double longtitude;

	public EarthLocation() { }

	public EarthLocation(double latitude, double longtitude) {
		if (-90 > latitude || latitude > 90
				|| -180 > longtitude || longtitude > 180) {
			throw new IllegalArgumentException("Illegal Position");
		}

		this.latitude = latitude;
		this.longtitude = longtitude;
	}


	public double getLatitude() {
		return latitude;
	}


	public void setLatitude(double latitude) {
		if (-90 > latitude || latitude > 90) {
			throw new IllegalArgumentException("Illegal Position");
		}

		this.latitude = latitude;
	}


	public double getLongtitude() {
		return longtitude;
	}


	public void setLongtitude(double longtitude) {
		if (-180 > longtitude || longtitude > 180) {
			throw new IllegalArgumentException("Illegal Position");
		}

		this.longtitude = longtitude;
	}

	@Override
	public String toString() {
		return latitude + "," + longtitude;
	}
}
