package jp.kitproject.kanavie;

import java.io.Serializable;


public class LandmarkDiff extends Landmark implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 2122339765934042222L;

	private double distance = 0;

	public LandmarkDiff(int id, int busStopId, String name, String detail, double latitude, double longtitude) {
		super(id, busStopId, name, detail, latitude, longtitude);
	}

	public LandmarkDiff(int id, int busStopId, String name, String detail, EarthLocation location) {
		super(id, busStopId, name, detail, location);
	}

	public LandmarkDiff(Landmark landmark, double distance) {
		super(landmark.getId(), landmark.getStandardBusStopId(), landmark.getName(), landmark.getDetail(), landmark.getLocation());

		this.distance = distance;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

}
