package jp.kitproject.kanavie;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import jp.kitproject.kanavie.network.HttpUtilities;

import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Environment;


public class ImageProvider {

	private static final String IMAGE_PLACE = "http://kfc9.usamimi.info/kanavie/image/";
	private static final String SDCARD_CACHE =
			Environment.getExternalStorageDirectory().toString() + "/Android/data/jp.kitproject.kanavie/cache/";

	public static Drawable getImage(int landmarkID) {
		Drawable result = null;

		String url = IMAGE_PLACE + String.valueOf(landmarkID) + ".jpg";
		String cache = SDCARD_CACHE + String.valueOf(landmarkID) + ".jpg";

		if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
			File cacheFile = new File(cache);
			if (cacheFile.exists()) {
				result = new BitmapDrawable(BitmapFactory.decodeFile(cacheFile.toString()));
			} else {
				byte[] data = HttpUtilities.getBinary(url);

				if (data != null) {
					cacheImage(cache, data);
					result = new BitmapDrawable(BitmapFactory.decodeByteArray(data, 0, data.length));
				}
			}
		}

		return result;
	}

	private static void cacheImage(String cache, byte[] data) {
		OutputStream out = null;

		try {
			new File(SDCARD_CACHE).mkdirs();

			out = new FileOutputStream(cache);
			out.write(data);
		} catch (FileNotFoundException e) {
		} catch (IOException e) {
		} finally {
			if (out != null) {
				try {
					out.close();
				} catch (IOException e) { }
			}
		}
	}
}
